import React, { Fragment } from 'react'
import NavBar from './components/NavBar'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Home from './pages/Home'
import About from './pages/About'

import './App.css'

const App = () => {
  return (
    <Router>
      <Fragment>
        <NavBar />
        <div className='container'>
          <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/about' component={About} />
          </Switch>
          ContactKeeper
        </div>
      </Fragment>
    </Router>
  )
}

export default App
